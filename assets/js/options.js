function replace_i18n(obj, tag) {
    var msg = tag.replace(/__MSG_(\w+)__/g, function(match, v1) {
        return v1 ? chrome.i18n.getMessage(v1) : '';
    });

    if(msg != tag) obj.innerHTML = msg;
}

function localizeHtmlPage() {
    // Localize using __MSG_***__ data tags
    var data = document.querySelectorAll('[data-localize]');

    for (var i in data) if (data.hasOwnProperty(i)) {
        var obj = data[i];
        var tag = obj.getAttribute('data-localize').toString();

        replace_i18n(obj, tag);
    }
}


// Saves options to chrome.storage
function save_options() {

  chrome.storage.sync.set({
    password_length: $('#password_length').attr('value'),
    alpha_numeric: ($('#alpha_numeric').prop("checked"))?"1":"0",
    require_upper: ($('#require_upper').prop("checked"))?"1":"0",
    require_number: ($('#require_number').prop("checked"))?"1":"0",
    require_lower: ($('#require_lower').prop("checked"))?"1":"0",
    require_symbol: ($('#require_symbol').prop("checked"))?"1":"0",
    first_char: $('#first_char').val(),
    special_chars: $("#special_chars").val()
  }, function() {
       // Update status to let user know options were saved.
        var status = $('.status');
        status.text('Options saved.');
        status.fadeIn();
        status.addClass('success_saved');
        setTimeout(function() {
          status.fadeOut();      
        },2000);
  });
  
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
  function restore_options() {
  chrome.storage.sync.get([
    'password_length',
    'alpha_numeric',
    'require_upper',
    'require_number',
    'require_lower',
    'require_symbol',
    'first_char',
    'special_chars'
  ], function(items) {
        $('#password_length').attr('value',items.password_length);
        $('.range-slider__value').html(items.password_length);
        input_rangeslider = $('#password_length');

     /**************** CHANGE RANGESLIDER VALUE ********************/
        if(typeof(input_rangeslider.value) != "undefined"){
           const fraction = (input_rangeslider.value - input_rangeslider.min) / (input_rangeslider.max - input_rangeslider.min);
           if (fraction === 0) {
             input_rangeslider.nextSibling.classList.add(cssClasses[0]);
            } else if (fraction === 1) {
             input_rangeslider.nextSibling.classList.add(cssClasses[1]);
            } else {
             input_rangeslider.nextSibling.classList.remove(...cssClasses)
           }
        }  
      /******** RESTORING THE REST FIELDS *************************/
        $('#alpha_numeric').prop('checked',((items.alpha_numeric=="1")?true:false));
        $('#require_upper').attr('checked',((items.require_upper=="1")?true:false));
        $('#require_number').attr('checked',((items.require_number=="1")?true:false));
        $('#require_lower').attr('checked',((items.require_lower=="1")?true:false));
        $('#require_symbol').attr('checked',((items.require_symbol=="1")?true:false));
        $('#first_char').val(items.first_char);
        $('#special_chars').val(items.special_chars);
  });
  
}
window.onload = restore_options();

//document.addEventListener('DOMContentLoaded', restore_options);
$(document).ready(function(){
   $('#lll').text(chrome.i18n.getMessage('@@ui_locale'));
   $( "#lang" ).change(function() {
      alert( "Handler for .change() called." );
  });
  localizeHtmlPage();
  $('#save').click(save_options);
});