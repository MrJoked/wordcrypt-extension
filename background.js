
// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
// Called when the user clicks on the browser action.


chrome.browserAction.onClicked.addListener(function(tab) {	  
  // Including in ActiveTab file to handle a pass after generating and jquery to load it into input
       chrome.tabs.executeScript(null, {file: "assets/js/handle_pass.js"});
       chrome.tabs.executeScript(null, {file: "assets/js/jquery.min.js"});
  // No tabs or host permissions needed!
  	var url = new URL(tab.url);
  	var domain = url.hostname;  
	   domain = domain.replace(/^(?:www\.)?/i, "");
  	 chrome.storage.sync.get({
    	password_length: '16',
    	alpha_numeric: '0',
    	require_upper: '0',
    	require_number: '0',
    	require_lower: '0',
    	require_symbol: '0',
    	first_char: 'random',
    	special_chars: 'include_any'
  		}, function(items) {		
				
        $('#login_username').wordcrypt({
					domain_name:     domain,
					password_length: items.password_length,
					first_char:      items.first_char,
					special_chars:   items.special_chars,
					alpha_numeric:   items.alpha_numeric,
					require_upper:   items.require_upper,
					require_lower:   items.require_lower,
					require_number:  items.require_number,
					require_symbol:  items.require_symbol,
          language:        chrome.i18n.getMessage('@@ui_locale') 
				}); 

			});        
});